﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    private Image frameSprite;



    private void Start()
    {
        frameSprite = GetComponent<Image>();
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        frameSprite.color = Color.blue;

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        frameSprite.color = Color.white;
    }

    /* public void OnDrop(PointerEventData eventData)
     {
         if (eventData.pointerDrag != null)
         {
             eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition
                 = frameSprite.rectTransform.anchoredPosition; ;
         }
     }*/
}
