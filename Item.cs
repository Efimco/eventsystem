﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;





public class Item : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private AudioSource audioSource;
    private RectTransform transform;
    private RectTransform dragFrom;
    private CanvasGroup group;
    private Item item;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        transform = GetComponent<RectTransform>();
        group = GetComponent<CanvasGroup>();
    }


    private void Update()
    {
        if (Input.GetMouseButtonUp(0) && item != null)
        {
            Drop(dragFrom);
        }

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        audioSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = new Vector3(1.5f, 1.5f, 1);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = new Vector3(1, 1, 1);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
        group.blocksRaycasts = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        dragFrom = GetComponent<RectTransform>();
        item = GetComponent<Item>();
    }

    void Drop(RectTransform transform)
    {
        gameObject.GetComponent<RectTransform>().SetParent(transform);
        gameObject.GetComponent<RectTransform>().anchoredPosition = transform.GetComponent<RectTransform>().anchoredPosition;
        item = null;

    }



    public void OnEndDrag(PointerEventData eventData)
    {
        group.blocksRaycasts = true;
    }
}
